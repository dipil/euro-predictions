require 'sinatra'
require 'haml'
require "net/https"
require "uri"

desc "This task is called by the Heroku scheduler add-on"

task :send_reminders do
  puts "Sending email..."

  api_key = ENV['MAILGUN_API_KEY']
  # postmaster@app160811bd2ad6499da7f9b8ec64cf88ef.mailgun.org
  app_name = ENV['MAILGUN_SMTP_LOGIN'].match(/postmaster@(.*)/)[1]
  api_url = URI.parse("https://api.mailgun.net/v2/#{app_name}/messages")
  http = Net::HTTP.new(api_url.host, api_url.port)
  http.use_ssl = true

  current_time = Time.now.utc

  Match.all(:kick_off_time.gte => current_time - 15 * 60, :kick_off_time.lte => current_time + 15* 60).each do |match|
    @match = match
    @predictions = @match.predictions.sort_by{|p| p.user.email }
    p "Sending email for ... #{@match.team_a} VS #{@match.team_b}"
    haml_template = File.read(File.join("./views/", 'match_predictions.haml'))

    data = {
      :from => "codysseynepal@gmail.com",
      :to => "dipil.saud@gmail.com",
      :cc => User.all.map(&:email),
      :subject => "#{@match.team_a} VS #{@match.team_b}",
      :html => Haml::Engine.new(haml_template).render(binding)
    }

    request = Net::HTTP::Post.new(api_url.request_uri)
    request.basic_auth("api", api_key)
    request.set_form_data(data)
    http.request(request)
  end

  puts "done."
end
