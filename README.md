Hello!
====

Sinatra application to add football games and allow users to make individual predictions to those games.

Scores are updated and leaderboards shown when the admin adds the results for the games.

Go!
===

Install Sinatra if you haven't already:

    sudo gem install sinatra

Download and run sinatra-bootstrap:

	git clone git@github.com:pokle/sinatra-bootstrap.git
	cd sinatra-bootstrap
	ruby app.rb
	
Then open [http://localhost:4567/](http://localhost:4567/)
