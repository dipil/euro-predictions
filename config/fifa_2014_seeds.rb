require "#{File.dirname(__FILE__)}/data.rb"
require 'open-uri'
require 'securerandom'
require 'rest_client'
require 'haml'

class SeedData

  DEFAULT_PASSWORD = "password"

  def self.get_matches
    url = "http://www.bbc.com/sport/football/world-cup/2014/schedule/group-stage"
    doc = Nokogiri::HTML(open url)
    doc.css(".group-stage").each do |gs|
      group = gs.at_css('h2').text
      hash = {}
      gs.css('.fixture__block-link').each do |li|
        team_a = li.at_css('.home_team').text.split.join(" ")
        team_b = li.at_css('.away_team').text.split.join(" ")
        formatted_date = li.at_css('.fixture__date').text
        scraped_date = Date.parse(formatted_date).strftime("%m/%d")
        scraped_time = li.at_css('.fixture__number--time').text
        kick_off_time = "#{scraped_date} #{scraped_time}"
        hash = { group: group, team_a:team_a , team_b: team_b, kick_off_time: kick_off_time }
        Match.create hash
      end
    end

  end

  def self.create_users
    users = %w( samesh3mikha@gmail.com
    manishlaldas.md@gmail.com
    amir@agrian.com
    pratuat@gmail.com
    shrey.is.shrey@gmail.com
    ranendra@agrian.com
    nirajktwl@gmail.com
    shilpakar.kris@gmail.com
    rohit@agrian.com
    thapa.abhay@gmail.com
    amit.15april@gmail.com
    dipeshgtm@gmail.com
    dipil.saud@gmail.com
    prasvinp@gmail.com
    sachin@agrian.com
    ck_gagan@yahoo.com
    niroj@geekhq.co
    )
    users.each do |email|
      @password = SecureRandom.uuid.split("-").last
      @user = User.create(email: email.strip, password: @password, password_confirmation: @password)

      #send email
      API_KEY = ENV['MAILGUN_API_KEY']
      API_URL = "https://api:#{API_KEY}@api.mailgun.net/v2/sandbox44c52e080e90452287f3b68af1b73dc6.mailgun.org"
      haml_template = File.read(File.join("./views/", 'sign_up.haml'))
      RestClient.post API_URL+"/messages",
        :from => "codysseynepal@gmail.com",
        :to => email,
        :subject => "Fifa-Pundits Login Credentials",
        :html => Haml::Engine.new(haml_template).render(binding)
    end
  end

  def roundOf16
    [{ group: 'Round of 16', team_a:"Brazil" , team_b:"Chile", kick_off_time:"06/28 16:00"  },
    { group: 'Round of 16', team_a:"Colombia" , team_b:"Uruguay", kick_off_time:"06/28 20:00"  },
    { group: 'Round of 16', team_a:"Netherlands" , team_b:"Mexico", kick_off_time:"06/29 16:00"  },
    { group: 'Round of 16', team_a:"Costa Rica" , team_b:"Greece", kick_off_time:"06/29 20:00"  },
    { group: 'Round of 16', team_a:"France" , team_b:"Nigeria", kick_off_time:"06/30 16:00"  },
    { group: 'Round of 16', team_a:"Germany" , team_b:"Algeria", kick_off_time:"06/30 20:00"  },
    { group: 'Round of 16', team_a:"Argentina" , team_b:"Switzerland", kick_off_time:"07/1 16:00"  },
    { group: 'Round of 16', team_a:"Belgium" , team_b:"USA", kick_off_time:"07/1 20:00"  }
    ].each do |match|
      Match.create match
    end
  end

  def quarterFinal
    [{ group: 'QuarterFinal', team_a:"France" , team_b:"Germany", kick_off_time:"07/4 16:00"  },
     { group: 'QuarterFinal', team_a:"Brazil" , team_b:"Colombia", kick_off_time:"07/4 20:00"  },
     { group: 'QuarterFinal', team_a:"Argentina", team_b:"Belgium", kick_off_time:"07/5 16:00"  },
     { group: 'QuarterFinal', team_a:"Netherlands", team_b:"Costa Rica", kick_off_time:"07/5 20:00"  }
    ].each do |match|
      Match.create match
    end
  end

  def semiFinal
    [{ group: 'SemiFinal', team_a:"Brazil" , team_b:"Germany", kick_off_time:"07/8 20:00"  },
     { group: 'SemiFinal', team_a:"Netherlands", team_b:"Argentina", kick_off_time:"07/9 20:00" }
    ].each do |match|
      Match.create match
    end
  end
end