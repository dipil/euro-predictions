require "#{File.dirname(__FILE__)}/data.rb"
require "net/https"
require "uri"
require 'haml'

class SeedData

  DEFAULT_PASSWORD = "123456"

  def self.create_matches
    Match.create(team_a: "Tottenham Hotspur", team_b: "Arsenal", group: "Week 1", kick_off_time: "02/7 17:00")
    Match.create(team_a: "Aston Villa", team_b: "Chelsea", group: "Week 1", kick_off_time: "02/7 17:00")
    Match.create(team_a: "Leicester City", team_b: "Crystal Palace", group: "Week 1", kick_off_time: "02/7 17:00")
    Match.create(team_a: "Manchester City ", team_b: "Hull City", group: "Week 1", kick_off_time: "02/7 17:00")
    Match.create(team_a: "QPR", team_b: "Southampton", group: "Week 1", kick_off_time: "02/7 17:00")
    Match.create(team_a: "Swansea City", team_b: "Sunderland", group: "Week 1", kick_off_time: "02/7 17:00")
    Match.create(team_a: "Everton", team_b: "Liverpool", group: "Week 1", kick_off_time: "02/7 17:00")
    Match.create(team_a: "Burnley", team_b: "West Bromwich Albion", group: "Week 1", kick_off_time: "02/7 17:00")
    Match.create(team_a: "Newcastle United", team_b: "Stoke City", group: "Week 1", kick_off_time: "02/7 17:00")
    Match.create(team_a: "West Ham United", team_b: "Manchester United", group: "Week 1", kick_off_time: "02/7 17:00")

    #Week 2
    Match.create(team_a: "Arsenal", team_b: "Leicester City", group: "Week 2", kick_off_time: "02/10 19:15")
    Match.create(team_a: "Hull City", team_b: "Aston Villa", group: "Week 2", kick_off_time: "02/10 19:15")
    Match.create(team_a: "Sunderland", team_b: "QPR", group: "Week 2", kick_off_time: "02/10 19:15")
    Match.create(team_a: "Liverpool", team_b: "Tottenham Hotspur", group: "Week 2", kick_off_time: "02/10 19:15")
    Match.create(team_a: "Chelsea", team_b: "Everton", group: "Week 2", kick_off_time: "02/11 19:15")
    Match.create(team_a: "Manchester United", team_b: "Burnley", group: "Week 2", kick_off_time: "02/11 19:15")
    Match.create(team_a: "Southampton", team_b: "West Ham United", group: "Week 2", kick_off_time: "02/11 19:15")
    Match.create(team_a: "Stoke City", team_b: "Manchester City", group: "Week 2", kick_off_time: "02/11 19:15")
    Match.create(team_a: "Crystal Palace", team_b: "Newcastle United", group: "Week 2", kick_off_time: "02/11 19:15")
    Match.create(team_a: "West Bromwich Albion", team_b: "Swansea City", group: "Week 2", kick_off_time: "02/11 19:15")

  end

  def self.create_users
    users = %w( samesh3mikha@gmail.com
    shrey.is.shrey@gmail.com
    thapa.abhay@gmail.com
    dipil.saud@gmail.com
    viswasthapa@googlemail.com
    gauravbasnyat@gmail.com
    nirajshrestha32@gmail.com
    )
    users.each do |email|
      password = SecureRandom.uuid.split("-").last
      self.create_user(email, password)
    end
  end

  def self.create_user(email, password)
    @password = password
    @user = User.create(email: email.strip, password: @password, password_confirmation: @password)
    #send email
    api_key = ENV['MAILGUN_API_KEY']

    # postmaster@app160811bd2ad6499da7f9b8ec64cf88ef.mailgun.org
    app_name = ENV['MAILGUN_SMTP_LOGIN'].match(/postmaster@(.*)/)[1]
    api_url = URI.parse("https://api.mailgun.net/v2/#{app_name}/messages")
    http = Net::HTTP.new(api_url.host, api_url.port)
    http.use_ssl = true
    haml_template = File.read(File.join("./views/", 'sign_up.haml'))
    data = {
      :from => "dipil.saud@gmail.com",
      :to => email,
      :subject => "EPL-Pundits Login Credentials",
      :html => Haml::Engine.new(haml_template).render(binding)
    }
    request = Net::HTTP::Post.new(api_url.request_uri)
    request.basic_auth("api", api_key)
    request.set_form_data(data)
    http.request(request)
  end

end
